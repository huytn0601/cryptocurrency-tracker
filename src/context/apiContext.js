import React, { useState, useEffect, createContext } from "react";

import axios from "axios";

export const APIContext = createContext();

export function APIContextProvider({children}) {
    const [data, setData] = useState([]);
    useEffect(() => {
        async function fetchData() {
            const { data } = await axios.get(
                'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false'
            )
            setData(data);
        }
        fetchData();
    }, []);
    
    return (
        <APIContext.Provider value = {{
            data
        }}
        >
            {children}
        </APIContext.Provider>
    );
}
