import React from "react";
import style from "./coin.module.css";

export default function Coin(props) {
    const { 
        icon, 
        name, 
        short, 
        price, 
        size, 
        changePercentage, 
        marketCap
    } = props;
    return(
        <div className={style.coinRow}>
            <div className={style.coinIcon}>
                <img className={style.coinIconImg} src ={icon} alt="Icon"/>
            </div>
            <div className={style.coinName}><p>{name}</p></div>
            <div className={style.coinShort}><p>{short}</p></div>
            <div className={style.coinPrice}><p>${price}</p></div>
            <div className={style.coinSize}><p>${size}</p></div>
            <div className={style.coinChangePercentage}>
                <p>{changePercentage}%</p>
            </div>
            <div className={style.coinMarketCap}><p>${marketCap}</p></div>
        </div>
    )
}