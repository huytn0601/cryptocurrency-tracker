import React, { useContext } from "react";
import style from "./coin.module.css";
import Coin from "./coin";
import { APIContext } from "../../context/apiContext";

export default function CoinList(props) {
    const { data } = useContext(APIContext);
    const { searchValue } = props;

    return (
        <div>
            {data.filter(value => value.id.includes(searchValue)).map((value, key) =>
                <Coin 
                    icon = {value.image}
                    name = {value.id}
                    short = {value.symbol}
                    price = {Math.floor(value.current_price * 100)/100}
                    size = {value.total_volume}
                    changePercentage = {Math.floor(value.price_change_percentage_24h * 100)/100}
                    marketCap = {value.market_cap}
                />
            )}
        </div>
    );
}