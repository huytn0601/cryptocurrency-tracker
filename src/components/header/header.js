import React from "react";
import style from "./header.module.css"

export default function Header() {
    return (
        <div className = {style.headerDiv}>
            <h1>Search a currency</h1>
        </div>
    );
}