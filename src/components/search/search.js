import React, { useState } from "react";
import style from "./search.module.css";

export default function Search(props) {
    const [search, setSearch] = useState([]);

    return(
        <div className={style.searchDiv}>
            <form 
                className={style.searchForm}
                value = {search}
                onSubmit ={props.submitSearch(search)}
            >
                <input 
                    className={style.searchInput}
                    type="text"
                    placeholder="Enter crypto..."
                    onChange={(e) => setSearch(e.target.value)}
                />
            </form>
        </div>
    )
}