import React, { useState } from 'react';
import './App.css';
import Header from './components/header/header';
import Search from './components/search/search';
import CoinList from './components/coins/coinList';
import { APIContextProvider } from './context/apiContext'

function App() {
  const [searchValue, setSearchValue] = useState([]);

  const submitSearch = (search) => {
    setSearchValue(search);
  }
  
  return (
    <APIContextProvider>
      <div className="App">
        <Header />
        <Search submitSearch = {submitSearch}/>
        <CoinList searchValue = {searchValue}/>
      </div>
    </APIContextProvider>
    
  );
}

export default App;
